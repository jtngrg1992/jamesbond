//
//  Globals.swift
//  JamesBond
//
//  Created by Jatin Garg on 27/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Globals {
    let xmppHost: String = "localhost" //192.168.0.110
    let xmppPort: uint_fast16_t = 5222
    let backgroundColor: UIColor = UIColor(colorLiteralRed: 23/255, green: 94/255, blue: 123/255, alpha: 1)
    let alternateBackgroungColor = UIColor(colorLiteralRed: 33/255, green: 100/255, blue: 128/255, alpha: 1)
    let btnBorderColor: UIColor = UIColor.white
    let btnTextColor: UIColor = UIColor.white
    let btnLineWidth: CGFloat = 2
    let btnfont: UIFont = UIFont(name: "Avenir Next", size: 15)!
    let textFiledBoundaryColor: UIColor = UIColor.white
    let secondaryColor = UIColor(colorLiteralRed: 131/255, green: 195/255, blue: 65/255, alpha: 1)
    let pictureBackGround: UIColor = UIColor.gray.withAlphaComponent(0.3)
    let navigationTitleFont = UIFont(name: "Avenir Next", size: 17)
    let placeholderColor = UIColor(colorLiteralRed: 118/255, green: 138/255, blue: 145/255, alpha: 1)
    func showMessage(message: String) -> UIAlertController
    {
        let alertVC = UIAlertController(title: "Attention", message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alertVC
    }
    func getAttributedPlaceholder(text: String, size: CGFloat) -> NSMutableAttributedString{
        let mutableString = NSMutableAttributedString(string: text, attributes: [NSFontAttributeName:UIFont(name: "Avenir Next", size: size) ?? UIFont.systemFont(ofSize: 15)])
        let color = placeholderColor
        mutableString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSRange(location: 0, length: text.characters.count))
        return mutableString
    }
    
    func configureTitleView(navigationController: UINavigationController?,title: String, subTitle: String,navigationItem: UINavigationItem){
        let v = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: (navigationController?.navigationBar.frame.height)! - 10))
        let titleLabel = FamLabel()
        titleLabel.text = title
        titleLabel.font = UIFont(name: "Avenir Next", size: 20)
        titleLabel.textAlignment = .center
        let subLabel = FamLabel()
        subLabel.textAlignment = .center
        subLabel.text = subTitle
        subLabel.font = UIFont(name: "Avenir Next", size: 10)
        subLabel.adjustsFontSizeToFitWidth = true
        subLabel.baselineAdjustment = .alignCenters
        v.addSubview(titleLabel)
        v.addSubview(subLabel)
        v.addConstraintWithFormat(format: "H:|[v0]|", views: titleLabel)
        v.addConstraintWithFormat(format: "H:|[v0]|", views: subLabel)
        v.addConstraintWithFormat(format: "V:|[v0][v1(10)]-5-|", views: titleLabel,subLabel)
        navigationItem.titleView = v
    }
    

    class var sharedInstance: Globals{
        struct Static{
            static let instance: Globals = Globals()
        }
        return Static.instance
    }
}
