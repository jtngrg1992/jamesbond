//
//  VerificationViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 28/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController , UITextFieldDelegate{
    
    
    @IBOutlet weak var pinContainerView: UIStackView!
    @IBOutlet weak var timerLabel: UILabel!
    
    var counter = 160
    var timer = Timer()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        pinContainerView.subviews.forEach { (v) in
            if let tF = v as? FamTextField{
                tF.keyboardType = .numberPad
                tF.delegate = self
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        pinContainerView.subviews.forEach { (tF) in
            if let textField = tF as? FamTextField{
                if textField.isFirstResponder{
                    textField.resignFirstResponder()
                }
            }
        }
    }
    
    func updateTimer()
    {
        if (counter > 0){
            counter -= 1
            timerLabel.text = "\(counter/60):\(counter%60)"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if range.length + range.location > currentCharacterCount{
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 1
    }
    

}
