//
//  ContactsViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 22/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import XMPPFramework

class ContactsViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate, XMPPStreamDelegate{
    
    var revealVC: SWRevealViewController!
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let c = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        c.translatesAutoresizingMaskIntoConstraints = false
        c.register(ContactsCell.self, forCellWithReuseIdentifier: "cell")
        c.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        c.alwaysBounceVertical = true
        return c
    }()
    
    var contacts: [Contact]!{
        didSet{
            collectionView.reloadData()
        }
    }
    override func viewDidLoad() {
        setupNavigationBar()
        setupCollectionView()
        fetchContacts()
        revealVC = tabBarController?.revealViewController()
        
        //XMPP Stuff
//        createNewXMPPAccount()
        
    }
    
    func createNewXMPPAccount(){
        let username = "admin@jlabs-win-pc"
        let password = "admin"
        let jid = XMPPJID(string: username)
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.xmppStream?.myJID = jid
        let allowSelfSignedCertificates = true
        let allowSSLHostNameMismatch = true
        var usrDefault = UserDefaults.standard
        usrDefault.set(username, forKey: "Account.JID")
        usrDefault.set("ios", forKey: "Account.Resource")
        usrDefault.set(true, forKey: "Account.UseSSL")
        usrDefault.set(true, forKey: "Account.AllowSelfSignedCert")
        usrDefault.set(true, forKey: "Account.AllowSSLHostNameMismatch")
        usrDefault.set(true, forKey: "Account.RememberPassword")
        usrDefault.set(password, forKey: "Account.Password")
        usrDefault.synchronize()
            var e: NSError? = nil
            do{
                try delegate.xmppStream?.register(withPassword: password)
            }catch{
                e = error as NSError?
            }
            var alert = UIAlertController()
            if e != nil{
                alert = Globals.sharedInstance.showMessage(message: "Erro registering: \(e)")
            }
            else{
                alert = Globals.sharedInstance.showMessage(message: "Success Registering")
            }
            present(alert, animated: true, completion: nil)
        
    }
    

    func xmppStreamDidRegister(_ sender: XMPPStream!) {
        print("IN REGISTER METHOD!!")
    }
    func xmppStream(_ sender: XMPPStream!, didNotRegister error: DDXMLElement!) {
        print("IN DID NOT REGISTER")
    }
    func setupNavigationBar(){
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        navigationController?.navigationBar.isTranslucent = false
        let label = FamLabel(frame: CGRect(x: 0, y: 0, width: 60, height: 70))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "CONTACTS"
        navigationItem.titleView = label
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-button"), style: .plain, target: revealVC, action: #selector(revealVC.revealToggle(_:)))
        
        navigationItem.leftBarButtonItem = btn
        let right = UIBarButtonItem(title: "SOS", style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = right
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 2
        layout.sectionInset.top = 5
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = layout
        view.addSubview(collectionView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
    }
    
    func fetchContacts(){
        let context = DBManager.getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        do{
            self.contacts = try context.fetch(fetchRequest) as? [Contact]
        }
        catch let err{
            print(err)
        }
    }
    }


//MARK :- Collection View Delegate & Data Source Methods
extension ContactsViewController{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ContactsCell
        cell.backgroundColor = Globals.sharedInstance.backgroundColor
        if let name = contacts[indexPath.item].name{
            cell.nameLabel.text = name
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
}

//MARK:- Supplementary Classes

class ContactsCell: UICollectionViewCell{
    var profilePicture: UIImageView = {
        let i = UIImageView()
        i.contentMode = .scaleAspectFill
        i.layer.cornerRadius = 30
        i.layer.masksToBounds = true
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "contact-picture")
        return i
    }()
    
    var containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var nameLabel: FamLabel = {
        let l = FamLabel()
        l.text = "Contact"
        l.font = UIFont(name: "AvenirNext-Bold", size: 17)
        return l
    }()
    
    var statusLabel: FamLabel = {
        let l = FamLabel()
        l.text = "Status"
        l.font = UIFont(name: "Avenir Next", size: 12)
        return l
    }()
    
    var callBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "call-btn"), for: .normal)
        return b
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        setupContainerView()
        addSubview(profilePicture)
        addSubview(containerView)
        addSubview(callBtn)
        addConstraintWithFormat(format: "H:|-10-[v0(60)]-10-[v1]-10-[v2(30)]-10-|", views: profilePicture,containerView,callBtn)
        addConstraintWithFormat(format: "V:[v0(60)]", views: profilePicture)
        addConstraintWithFormat(format: "V:[v0(30)]", views: callBtn)
        addConstraintWithFormat(format: "V:[v0(70)]", views: containerView)
        addConstraint(NSLayoutConstraint(item: profilePicture, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: callBtn, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        
    }
    func setupContainerView(){
        containerView.addSubview(nameLabel)
        containerView.addSubview(statusLabel)
        containerView.addConstraintWithFormat(format: "H:|[v0]|", views: nameLabel)
        containerView.addConstraintWithFormat(format: "H:|[v0]|", views: statusLabel)
        containerView.addConstraintWithFormat(format: "V:|-10-[v0]-2-[v1]", views: nameLabel,statusLabel)
        addConstraint(NSLayoutConstraint(item: containerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
}
