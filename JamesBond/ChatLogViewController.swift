//
//  ChatLogViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 18/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import XMPPFramework

class ChatLogViewController: UIViewController, UIViewControllerTransitioningDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate{
    var bondTitle: String!
    let interactor = Interactor()
    var friendsToDisplay = [String]()
    var bondToDisplay: Bond!{
        didSet{
            bondToDisplay.members?.forEach({ (member) in
                let mem = member as! Contact
                friendsToDisplay.append(mem.name!)
            })
        }
    }
    
    var messages = [Message](){
        didSet{
            collectionView.reloadData()
        }
    }
    
    //MARK :- XMPP Stuff
    
    lazy var fetchedResultsController: NSFetchedResultsController = { () -> NSFetchedResultsController<NSFetchRequestResult> in
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "sent", ascending: true)]
        let frc:NSFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchReq, managedObjectContext: DBManager.getContext(), sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }()
    
    var blockOperations = [BlockOperation]()
    //MARK :- UI Elements
    var collectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        let coll = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        coll.translatesAutoresizingMaskIntoConstraints = false
        coll.alwaysBounceVertical = true
        coll.backgroundColor = UIColor.white
        coll.register(ChatLogCell.self, forCellWithReuseIdentifier: "cell")
        return coll
    }()
    
    var inputTextField : FamTextField = {
        let t = FamTextField()
        t.strokeColor = UIColor(white: 0.65, alpha: 1)
        t.backgroundColor = UIColor.white
        t.layer.cornerRadius = 5
        t.layer.masksToBounds = true
        t.textColor = UIColor.black
        t.translatesAutoresizingMaskIntoConstraints = false
        return t
    }()
    
    lazy var addBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("+", for: .normal)
        b.showsTouchWhenHighlighted = true
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        b.addTarget(self, action: #selector(toggleDropDown), for: .touchUpInside)
        return b
    }()
    
    lazy var sendBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.layer.cornerRadius = 15
        b.backgroundColor = Globals.sharedInstance.secondaryColor
        b.setImage(UIImage(named: "sent-mail"), for: .normal)
        b.addTarget(self, action: #selector(sendMsg), for: .touchUpInside)
        return b
    }()
    
    var textInputContainer: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = Globals.sharedInstance.backgroundColor
        return v
    }()
    
    lazy var pan: UIPanGestureRecognizer? = {
        let p = UIPanGestureRecognizer(target: self, action: #selector(viewPanned))
        return p
    }()
    
    var hidesBackButton: Bool = {
        return true
    }()
    
    var topBorderView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(white: 0.5, alpha: 1)
        return v
    }()
    
    var dropMenu: DropDownMenu = {
        let v = DropDownMenu()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 7
        v.layer.masksToBounds =  true
        v.backgroundColor = UIColor.white.withAlphaComponent(0.86)
        return v
    }()
    
    var bottomContraint: NSLayoutConstraint!
    var dropDownBottomConstraint: NSLayoutConstraint!
    var dropDownShowing = false
    var dropDownHeight: CGFloat = 170
    override func viewDidLoad() {
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        loadBond()
        Globals.sharedInstance.configureTitleView(navigationController: navigationController, title: bondTitle, subTitle: friendsToDisplay.joined(separator: ","), navigationItem: navigationItem)
        
        configureNavigationBar()
        if let p = pan{
            view.addGestureRecognizer(p)
        }
        
        interactor.hasStarted = true
        setUpCollectionView()
        setUpInputContainer()
        //KeyBoard Handler
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillHide, object: nil)
        setupDropDownMenu()
        fetchedResultsController.delegate = self
        //XMPP 
        do{
            try fetchedResultsController.performFetch()
        }catch {
            print("Error Fetching : \(error)")
        }
    }
    
    func sendMsg(){
        let msg = inputTextField.text
        if (msg?.characters.count)! > 0{
            let username = "admin@localhost"
            let jid = XMPPJID(string: username)
            let message = XMPPMessage(type: "chat", to: jid)
            message?.addBody(msg)
//            message?.addSubject(bondToDisplay.objectID.uriRepresentation().absoluteString)
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.xmppStream?.send(message)
            
            //creating message
            let coreMesage = NSEntityDescription.insertNewObject(forEntityName: "Message", into: DBManager.getContext()) as! Message
            coreMesage.text = msg!
            coreMesage.isSender = true
            coreMesage.sent = NSDate()
            coreMesage.isBondMsg = true
            coreMesage.bond = bondToDisplay
            DBManager.saveContext()
        }
        
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == NSFetchedResultsChangeType.insert{
            blockOperations.append(BlockOperation(block: { 
                self.collectionView.insertItems(at: [newIndexPath!])
            }))
        }
    }
   
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({ 
            for operation in self.blockOperations{
                operation.start()
            }
        }, completion: {(completed) in
            let lastItem = self.fetchedResultsController.sections![0].numberOfObjects - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        })
    }
    func setupDropDownMenu(){
        view.insertSubview(dropMenu, belowSubview: textInputContainer)
        dropDownBottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: dropMenu, attribute: .bottom, multiplier: 1, constant: -200)
        view.addConstraintWithFormat(format: "H:|-5-[v0]-5-|", views: dropMenu)
        view.addConstraintWithFormat(format: "V:[v0(>=\(dropDownHeight))]", views: dropMenu)
        view.addConstraint(dropDownBottomConstraint)
        
    }
    
    func toggleDropDown(sender: UIButton){
        if !dropDownShowing{
            dropDownShowing = true
            dropDownBottomConstraint.constant = 50
        }
        else{
            dropDownShowing = false
            dropDownBottomConstraint.constant = -dropDownHeight
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        
        
    }
    func handleKeyboardShow(notification: Notification){
        if let userinfo = notification.userInfo{
            let keyboardFrame = (userinfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let isKeyBoardShowing = notification.name == .UIKeyboardWillShow
            bottomContraint.constant = isKeyBoardShowing ? (keyboardFrame?.height)! : 0
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: {(complete) in
                //let lastIndexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                //self.collectionView.scrollToItem(at: lastIndexPath as IndexPath, at: .bottom, animated: true)
            })
        }
    }
    func setTabBarVisible(visible: Bool, animated: Bool) {
        // hide tab bar
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        print ("offsetY = \(offsetY)")
        
        // zero duration means no animation
        let duration:TimeInterval = (animated ? 0.3 : 0.0)
        
        // animate tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY!)
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
                return
            }
        }
    }
    
    func setUpCollectionView(){
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 2
        layout.sectionInset.top = 5
        layout.itemSize = CGSize(width: view.frame.width, height: 70)
        collectionView.collectionViewLayout = layout
        view.addSubview(collectionView)
        view.addSubview(textInputContainer)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: textInputContainer)
        view.addConstraintWithFormat(format: "V:|[v0]", views: collectionView)
        view.addConstraintWithFormat(format: "V:[v0(50)]", views: textInputContainer)
        bottomContraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: textInputContainer, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: textInputContainer, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(bottomContraint)
        collectionView.delegate = self
        collectionView.dataSource = self
        setUpInputContainer()
    }
    
    func setUpInputContainer(){
        textInputContainer.addSubview(topBorderView)
        textInputContainer.addSubview(inputTextField)
        textInputContainer.addSubview(addBtn)
        textInputContainer.addSubview(sendBtn)
        
        textInputContainer.addConstraintWithFormat(format: "H:|[v0]|", views: topBorderView)
        textInputContainer.addConstraintWithFormat(format: "V:|[v0(1)]", views: topBorderView)
        textInputContainer.addConstraintWithFormat(format: "H:|[v0(30)][v1]-5-[v2(30)]-5-|", views: addBtn,inputTextField,sendBtn)
        textInputContainer.addConstraintWithFormat(format: "V:|-5-[v0]-5-|", views: inputTextField)
        textInputContainer.addConstraintWithFormat(format: "V:|[v0]|", views: addBtn)
        textInputContainer.addConstraintWithFormat(format: "V:[v0(30)]", views: sendBtn)
        textInputContainer.addConstraint(NSLayoutConstraint(item: textInputContainer, attribute: .centerY, relatedBy: .equal, toItem: sendBtn, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func viewPanned(panGesture: UIPanGestureRecognizer){
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let nav = sb.instantiateViewController(withIdentifier: "swreveal") as! SWRevealViewController
            nav.frontViewController = sb.instantiateViewController(withIdentifier: "applicationentry")
            nav.rearViewController = sb.instantiateViewController(withIdentifier:
                "rearVC")

            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
            interactor.hasStarted = true
            
        case .changed:
            interactor.update(percent)
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                interactor.finish()
                interactor.hasStarted = false
            } else {
                interactor.cancel()
            }
            
        case .cancelled, .failed:
            interactor.hasStarted = false
            interactor.cancel()
            
        default:
            break
        }

    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func configureNavigationBar(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        if hidesBackButton{
            navigationItem.hidesBackButton = true
            let left = UIBarButtonItem(image: UIImage(named: "back")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(goBack))
            navigationItem.leftBarButtonItem = left
        }
        
    }
    
    func goBack(sender: UIBarButtonItem){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nav = sb.instantiateViewController(withIdentifier: "swreveal") as! SWRevealViewController
        nav.frontViewController = sb.instantiateViewController(withIdentifier: "applicationentry")
        nav.rearViewController = sb.instantiateViewController(withIdentifier: "rearVC")
        nav.transitioningDelegate = self
        interactor.hasStarted = false
        present(nav, animated: true, completion: nil)
    }
    
    func loadBond(){
        let context = DBManager.getContext()
        print("Fetching data for \(bondTitle)")
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bond")
        do{
            let fetchedBonds = try context.fetch(fetchRequest)
            fetchedBonds.forEach({ (fetchedBond) in
                if let bond = fetchedBond as? Bond{
                    if(bond.title! == bondTitle!){
                        print("found")
                        self.bondToDisplay = bond
                    }
                }
            })
        }
        catch let err{
            print(err)
        }
}    
    
    func getEstimatedFrame(msg: String!) -> CGRect{
        let size = CGSize(width: view.bounds.width - 100, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: msg!).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15) ], context: nil)
        return estimatedFrame
    }
}


//MARK :- Extension for implementing delegate methods
extension ChatLogViewController{
    //MARK: - transition animation delegates
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("loading")
        return CustomTransition()
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        print(interactor.hasStarted)
        return interactor.hasStarted ? interactor : nil
    }

    //MARK :- Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = fetchedResultsController.sections?[0].numberOfObjects{
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ChatLogCell
        let message = fetchedResultsController.object(at: indexPath) as! Message
        let msg = message.text
        let estimatedFrame = getEstimatedFrame(msg: msg)
        cell.message.text = msg
        if !message.isSender{
            cell.message.frame = CGRect(x: 40 + 10 + 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
            cell.msgBubbleView.frame = CGRect(x: 40 + 10, y: 0, width: estimatedFrame.width + 20 + 8, height: estimatedFrame.height + 20)
            cell.chatBubbleSender.isHidden = true
            cell.chatBubble.isHidden = false
            cell.profileImgView.isHidden = false
        }
        else{
            cell.message.frame = CGRect(x: view.frame.width - estimatedFrame.width - 16 - 10 - 10, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
            cell.msgBubbleView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 16 - 10 - 8 - 8, y: 0, width: estimatedFrame.width + 20 + 8, height: estimatedFrame.height + 20)
            cell.chatBubbleSender.isHidden = false
            cell.chatBubble.isHidden = true
            cell.profileImgView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let message = fetchedResultsController.object(at: indexPath) as! Message
        let msg = message.text

        let estimatedFrame = getEstimatedFrame(msg: msg!)
        return CGSize(width: view.frame.width, height: estimatedFrame.height + 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
        if dropDownShowing{
            dropDownBottomConstraint.constant = -dropDownHeight
            dropDownShowing = false
        }
        
    }
    
    
    
    
}

//MARK :- Cell Class

class ChatLogCell: UICollectionViewCell{
    var message: UITextView = {
        let m = UITextView()
        m.translatesAutoresizingMaskIntoConstraints = false
        m.font = UIFont.systemFont(ofSize: 15)
        m.backgroundColor = UIColor.clear
        m.isEditable = false
        m.text = "Sample Message"
        return m
    }()
    
    var msgBubbleView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        v.layer.cornerRadius = 15
        v.layer.masksToBounds = true
        return v
    }()
    
    var profileImgView: UIImageView = {
        let i = UIImageView()
        i.contentMode = .scaleToFill
        i.translatesAutoresizingMaskIntoConstraints = false
        i.layer.cornerRadius = 15
        i.layer.masksToBounds = true
        i.image = UIImage(named: "contact-picture")
        return i
    }()
    
    
    var chatBubble: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "bubble_gray")!.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
        i.tintColor = Globals.sharedInstance.secondaryColor
        return i
        
    }()
    
    var chatBubbleSender: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "bubble_blue")!.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
        i.tintColor = UIColor(white: 0.7, alpha: 1)
        return i
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        addSubview(profileImgView)
        addSubview(msgBubbleView)
        addSubview(message)
        
        addConstraintWithFormat(format: "H:|-10-[v0(30)]", views: profileImgView)
        addConstraintWithFormat(format: "V:[v0(30)]|", views: profileImgView)
        
        msgBubbleView.addSubview(chatBubble)
        msgBubbleView.addSubview(chatBubbleSender)
        msgBubbleView.addConstraintWithFormat(format: "H:|[v0]|", views: chatBubble)
        msgBubbleView.addConstraintWithFormat(format: "V:|[v0]|", views: chatBubble)
        msgBubbleView.addConstraintWithFormat(format: "H:|[v0]|", views: chatBubbleSender)
        msgBubbleView.addConstraintWithFormat(format: "V:|[v0]|", views: chatBubbleSender)
        
    }
    
    
}

//MARK :- Supplementary Classes

class DropDownMenu: UIView{
    var pollBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "poll"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var soundBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "speakers"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var contactBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "phone-book"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var callenderBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "calender"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var docBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "document"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var locationBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = Globals.sharedInstance.backgroundColor
        b.setImage(UIImage(named: "location"), for: .normal)
        b.showsTouchWhenHighlighted = true
        b.layer.cornerRadius = 25
        return b
    }()
    
    var pollLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.textColor = UIColor.black
        l.text = "Poll"
        return l
    }()
    
    var soundLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.textColor = UIColor.black
        l.text = "Sound"
        return l
    }()

    var contactLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.textColor = UIColor.black
        l.text = "Contact"
        return l
    }()

    var calenderLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = UIColor.black
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.text = "Calender"
        return l
    }()

    var docLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = UIColor.black
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.text = "Document"
        return l
    }()

    var locLbl: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont(name: "Avenir Next", size: 12)
        l.textColor = UIColor.black
        l.text = "Location"
        return l
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(pollBtn)
        addSubview(soundBtn)
        addSubview(contactBtn)
        addSubview(callenderBtn)
        addSubview(docBtn)
        addSubview(locationBtn)
        addSubview(pollLbl)
        addSubview(soundLbl)
        addSubview(contactLbl)
        addSubview(calenderLbl)
        addSubview(docLbl)
        addSubview(locLbl)
        
        addConstraintWithFormat(format: "H:|-50-[v0(50)][v1(50)][v2(50)]-50-|", views: pollBtn,soundBtn,contactBtn)
        addConstraintWithFormat(format: "H:|-50-[v0(50)][v1(50)][v2(50)]-50-|", views: callenderBtn,docBtn,locationBtn)
//        addConstraintWithFormat(format: "H:|-50-[v0(50)][v1(50)][v2(50)]-50-|", views: pollLbl,soundLbl,contactLbl)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: soundBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: docBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraintWithFormat(format: "V:|-10-[v0(50)]-4-[v1]-10-[v2(50)]-4-[v3]", views: pollBtn,pollLbl,callenderBtn,calenderLbl)
        addConstraintWithFormat(format: "V:|-10-[v0(50)]-4-[v1]-10-[v2(50)]-4-[v3]", views: soundBtn,soundLbl,docBtn,docLbl)
        addConstraintWithFormat(format: "V:|-10-[v0(50)]-4-[v1]-10-[v2(50)]-4-[v3]", views: contactBtn,contactLbl,locationBtn,locLbl)
        
        //MARK:- Horizontal Constraints for labels
        addConstraint(NSLayoutConstraint(item: pollBtn, attribute: .centerX, relatedBy: .equal, toItem: pollLbl, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: soundLbl, attribute: .centerX, relatedBy: .equal, toItem: soundBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contactLbl, attribute: .centerX, relatedBy: .equal, toItem: contactBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: calenderLbl, attribute: .centerX, relatedBy: .equal, toItem: callenderBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: docLbl, attribute: .centerX, relatedBy: .equal, toItem: docBtn, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: locLbl, attribute: .centerX, relatedBy: .equal, toItem: locationBtn, attribute: .centerX, multiplier: 1, constant: 0))
        
    }
}

class XMessage: XMPPMessage{
    var bond: Bond!
}
