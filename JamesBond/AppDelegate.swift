//
//  AppDelegate.swift
//  JamesBond
//
//  Created by Jatin Garg on 27/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import Contacts
import CoreData
import XMPPFramework

protocol ChatDelegate {
    func buddyWentOnline(name: String)
    func buddyWentOffline(name: String)
    func didDisconnect()
}

@objc protocol XMPPDelegate{
    @objc optional func registerationSuccess(status: Bool)
    @objc optional func loginSuccess(status: Bool)
    @objc optional func connectionSuccess(status: Bool)
    @objc optional func recievedMessage(message: [String : Any])
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, XMPPRosterDelegate, XMPPStreamDelegate {

    var window: UIWindow?
    var contacts = CNContactStore()
    
    //XMPP Components
    var delegate: ChatDelegate! = nil
    let xmppStream = XMPPStream()
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster!
    var xmppDelegate: XMPPDelegate? = nil
    
    override init() {
        super.init()
        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
    }
    //End Xmpp Components
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        
        //checking for userDefaults
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "appInitialized") as? Bool) != nil{
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let initialVC = sb.instantiateViewController(withIdentifier: "swreveal") as! SWRevealViewController
            initialVC.frontViewController = sb.instantiateViewController(withIdentifier: "applicationentry")
            initialVC.rearViewController = sb.instantiateViewController(withIdentifier:
            "rearVC")
            self.window?.rootViewController = initialVC
            self.window?.makeKeyAndVisible()
        }
        else{
            print("No defaults Set")
        }
        
        //XMPP Component
        DDLog.add(DDTTYLogger.sharedInstance())
        setupStream()
        return true
    }
    
    func getDelegate() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func showMessage(message: String){
        let alertController = UIAlertController(title: "Attention!", message: message, preferredStyle:.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(action)
        let pushedVCs = (self.window?.rootViewController as! UINavigationController).viewControllers
        let presentedVC = pushedVCs[pushedVCs.count - 1]
        presentedVC.present(alertController, animated: true
            , completion: nil)
    }
    
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void){
        let authStatus = CNContactStore.authorizationStatus(for: .contacts)
        switch authStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            self.contacts.requestAccess(for: .contacts, completionHandler: { (access, accessError) in
                if access{
                    completionHandler(true)
                }
                else{
                    if authStatus == CNAuthorizationStatus.denied{
                        DispatchQueue.main.async {
                            let message = "\(accessError!.localizedDescription)\n Please allow the app to access your contacts in settings."
                            self.showMessage(message: message)
                            
                        }
                    }
                }
            })
        default:
            completionHandler(false)
        }
    }
    
    func retrieveContacts() -> [CNContact]{
            let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey] as [Any]
            var containers = [CNContainer]()
        do{
            containers = try self.contacts.containers(matching: nil)
        }
        catch{
            print("error fetching containers")
        }
        
        var results = [CNContact]()
        containers.forEach { (container) in
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do{
                let containerResults = try self.contacts.unifiedContacts(matching: fetchPredicate, keysToFetch: keys as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            }
            catch{
                print("error fetching results from container")
            }
            
        }
        return results
}
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        disconnect()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if (UserDefaults.standard.object(forKey: "appInitialized") != nil){
            if (connect()){
                goOnline()
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        DBManager.saveContext()
    }
    
    //MARK :- XMPP Methods
    
    private func setupStream(){
        xmppRoster.activate(xmppStream)
        xmppStream?.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppStream?.hostName = Globals.sharedInstance.xmppHost
        xmppStream?.hostPort = Globals.sharedInstance.xmppPort
    }
    
    private func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream?.myJID.domain
        print("Domain: \(domain)")
        if domain == "gmail.com" || domain == "gtalk.com" || domain == "talk.google.com" {
            let priority = DDXMLElement.element(withName: "priority", stringValue: "24") as! DDXMLElement
            presence?.addChild(priority)
        }
        xmppStream?.send(presence)
    }
    
    private func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream?.send(presence)
    }
    
    func connect() -> Bool {
        if !(xmppStream?.isConnected())! {
            let jabberID = UserDefaults.standard.string(forKey: "userID")
            let myPassword = UserDefaults.standard.string(forKey: "userPassword")
            
            if !(xmppStream?.isDisconnected())! {
                return true
            }
            if jabberID == nil && myPassword == nil {
//                jabberID = "admin@jlabs-win-pc"
//                myPassword = "admin"
                return false
            }
            
            xmppStream?.myJID = XMPPJID(string: jabberID)
            var error: Error? = nil
            do {
                try xmppStream?.connect(withTimeout: XMPPStreamTimeoutNone)
//                try xmppStream?.oldSchoolSecureConnect(withTimeout: XMPPStreamTimeoutNone)
            } catch let e {
                error = e
            }
            
            if error != nil{
                print("Erorr Encountered Connecting : \(error)")
                return false
            }else{
                print("Connection Success")
                return true
            }
        }
        return false
    }
    
    func disconnect() {
        goOffline()
        xmppStream?.disconnect()
    }
    
    //MARK: XMPP Delegates
    func xmppStreamDidConnect(_ sender: XMPPStream!) {
        xmppDelegate?.connectionSuccess!(status: true)
        print("Loggin in")
        do {
            try	xmppStream?.authenticate(withPassword: UserDefaults.standard.string(forKey: "userPassword"))
        } catch let e{
            print("Could not authenticate")
            window?.rootViewController?.present(Globals.sharedInstance.showMessage(message: "Exception occured while connecting stream : \(e)"), animated: true, completion: nil)
        }
    }
    
    func xmppStreamConnectDidTimeout(_ sender: XMPPStream!) {
        print("Connection timed out")
    }
    func xmppStreamDidAuthenticate(_ sender: XMPPStream!) {
        goOnline()
        xmppDelegate?.loginSuccess!(status: true)
    }
    func xmppStream(_ sender: XMPPStream!, didNotAuthenticate error: DDXMLElement!) {
        print("Authentication Error : \(error)")
        do{
            print("Trying to register with password \(UserDefaults.standard.string(forKey: "userPassword"))")
            try xmppStream?.register(withPassword: UserDefaults.standard.string(forKey: "userPassword"))
            
        }catch let e{
            print("Error: \(e)")
        }
        
    }
    func xmppStreamDidRegister(_ sender: XMPPStream!) {
        print("Registeration Success")
        xmppDelegate?.registerationSuccess!(status: true)
        var error: Error? = nil
        do{
            try xmppStream?.authenticate(withPassword: UserDefaults.standard.string(forKey: "userPassword"))
        }catch let e{
            error = e
        }
        if (error != nil){
            //some error
        }
        
    }
    func xmppStream(_ sender: XMPPStream!, didNotRegister error: DDXMLElement!) {
        print("Error Registering : \(error)")
        xmppDelegate?.registerationSuccess!(status: false)
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive iq: XMPPIQ!) -> Bool {
        print("Did receive IQ")
        return false
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive message: XMPPMessage!) {
//        print("Did receive message \(message)")
        let msg = message.body()
        let from = message.from()
        let coreMessage = NSEntityDescription.insertNewObject(forEntityName: "Message", into: DBManager.getContext()) as! Message
        coreMessage.isSender = false
        coreMessage.sent = NSDate()
        coreMessage.text = msg
        
        if message.body() != "Welcome to this XMPP server." && message.subject() != nil{
            let bondUrl = URL(string: message.subject())
            let bondID = DBManager.getContext().persistentStoreCoordinator?.managedObjectID(forURIRepresentation: bondUrl!)
            let fetchedBond = DBManager.getContext().object(with: bondID!) as! Bond
            coreMessage.isBondMsg = true
            fetchedBond.addToMessages(coreMessage)
        }
        DBManager.saveContext()
        let message: [String : Any] = ["message" : msg, "sender" : from]
        print(message)
        xmppDelegate?.recievedMessage!(message: message)
    }
    
    private func xmppStream(sender: XMPPStream!, didSendMessage message: XMPPMessage!) {
        print("Did send message \(message)")
        let ms = message as! XMessage
        print(ms.bond)
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive presence: XMPPPresence!) {
        let presenceType = presence.type()
        print("presence type: \(presenceType)")
        let myUsername = sender.myJID.user
        let presenceFromUser = presence.from().user
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(presenceFromUser)")
            if presenceType == "available" {
                delegate.buddyWentOnline(name: "\(presenceFromUser)@gmail.com")
            } else if presenceType == "unavailable" {
                delegate.buddyWentOffline(name: "\(presenceFromUser)@gmail.com")
            }
        }
    }
    
    func xmppRoster(_ sender: XMPPRoster!, didReceiveRosterItem item: DDXMLElement!) {
        print("Did receive Roster item")
    }
    
    
    
    


}

