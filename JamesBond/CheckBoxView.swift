//
//  CheckBoxView.swift
//  JamesBond
//
//  Created by Jatin Garg on 28/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import QuartzCore

 class CheckBoxView: UIView {
    var isChecked: Bool = false{
        didSet{
            setNeedsDisplay()
        }
    }
    
    
    let pathLayer = CAShapeLayer()
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        Globals.sharedInstance.backgroundColor.setFill()
        path.fill()
        if isChecked{
            print("Checking Marks")
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0.1 * bounds.width, y: bounds.height/2 + 0.2 * bounds.height ))
            path.addLine(to: CGPoint(x: bounds.width/2, y: bounds.height - 0.1 * bounds.height))
            path.addLine(to: CGPoint(x: bounds.width, y: bounds.height/2 - 0.4 * bounds.height))
            
            //creating sublayer
            
            pathLayer.frame = bounds
            pathLayer.path = path.cgPath
            pathLayer.lineWidth = 4
            pathLayer.strokeColor = Globals.sharedInstance.secondaryColor.cgColor
            pathLayer.fillColor = nil
            pathLayer.lineJoin = kCALineJoinBevel
            self.layer.addSublayer(pathLayer)
            
            //animating
            let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
            pathAnimation.duration = 2
            pathAnimation.fromValue = 0.0
            pathAnimation.toValue = 1.0
            pathLayer.add(pathAnimation, forKey: "strokeEnd")
        }
        else{
            pathLayer.removeFromSuperlayer()
        }
    }
    
    

}
