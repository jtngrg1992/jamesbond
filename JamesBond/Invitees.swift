//
//  Invitees.swift
//  JamesBond
//
//  Created by Jatin Garg on 01/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation

class Invitees{
    var phoneNumber: String? = nil
    var name: String? = nil
    var existingContact: Bool = false
    
    init(num: String?, name: String?) {
        self.phoneNumber = num
        self.name = name
    }
}
