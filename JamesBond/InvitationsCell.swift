//
//  InvitationsCell.swift
//  JamesBond
//
//  Created by Jatin Garg on 01/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import BEMCheckBox
class InvitationsCell: UICollectionViewCell {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var tickView: BEMCheckBox!
    
    var checked: Bool = false{
        didSet{
            tickView.setOn(checked, animated: true)
        }
    }
    
    override func awakeFromNib() {
        contactImage.layer.masksToBounds = true
        contactImage.layer.cornerRadius = contactImage.bounds.width/2
        imageContainerView.layer.cornerRadius = imageContainerView.bounds.width/2
        imageContainerView.layer.borderColor = Globals.sharedInstance.secondaryColor.cgColor
        imageContainerView.layer.borderWidth = 2
        
    }
}
