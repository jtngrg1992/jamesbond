//
//  Interactor.swift
//  JamesBond
//
//  Created by Jatin Garg on 20/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class Interactor: UIPercentDrivenInteractiveTransition{
    var hasStarted = false
    var shouldFinish = false
}
