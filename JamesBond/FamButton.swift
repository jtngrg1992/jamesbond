//
//  FamButton.swift
//  JamesBond
//
//  Created by Jatin Garg on 27/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

 class FamButton: UIButton {
    
     var cornerRadius: CGFloat = 0{
        didSet{
            setNeedsDisplay()
        }
    }
        override func draw(_ rect: CGRect) {
//        let path = UIBezierPath(rect: rect)
        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        Globals.sharedInstance.btnBorderColor.setStroke()
        path.lineWidth = Globals.sharedInstance.btnLineWidth
        Globals.sharedInstance.backgroundColor.setFill()
        path.fill()
        path.stroke()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        titleEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = Globals.sharedInstance.btnfont
    }
    
    
}
