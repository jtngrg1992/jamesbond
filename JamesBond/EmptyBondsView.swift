//
//  EmptyBondsView.swift
//  JamesBond
//
//  Created by Jatin Garg on 14/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class EmptyBondsBackground: UIView{
    
    var scale: CGFloat = 0.9{didSet{setNeedsDisplay()}}
    var mouthCurvature: Double = -1.0{didSet{setNeedsDisplay()}}
    var eyesOpen: Bool = true{didSet{setNeedsDisplay()}}
    var lineWidth: CGFloat = 3.0{didSet{setNeedsDisplay()}}
    let strokeColor: UIColor = UIColor(colorLiteralRed: 45/255, green: 88/255, blue: 106/255, alpha: 1)
    private var skullRadius: CGFloat{
        return 50
    }
    
    private var skullCenter: CGPoint{
        return CGPoint(x: bounds.midX, y:bounds.minY + 80)
    }
    
    let message : UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir Next", size: 20)
        label.text = "Looks like there's no one to talk to \n Start a new bond"
        label.textAlignment = .center
        label.textColor = UIColor(colorLiteralRed: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        return label
    }()
    private func getPathForDrawingSkull(radius: CGFloat, center: CGPoint)->UIBezierPath{
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: 0.0, endAngle: CGFloat(2*M_PI), clockwise: true)
        path.lineWidth = lineWidth
        return path
    }
    
    private func getPathFordrawingEye(eye: Eye)->UIBezierPath{
        let eyeRadius = skullRadius/ratios.SkullRadiusToEyeRadius
        let eyeCenter = getEyeCenter(eye: eye)
        if eyesOpen{
            let path = getPathForDrawingSkull(radius: eyeRadius, center: eyeCenter)
            return path
        }
        else{
            let start = CGPoint (x: eyeCenter.x - eyeRadius, y: eyeCenter.y)
            let end = CGPoint (x: eyeCenter.x + eyeRadius, y: eyeCenter.y)
            let path =
                UIBezierPath()
            path.move(to: start)
            path.addLine(to: end)
            path.lineWidth = lineWidth
            return path
            
        }
        
    }
    
    private func getPathForDrawingMouth()->UIBezierPath{
        let mouthOffset = skullRadius/ratios.SkullRadiusToMouthOffset
        let mouthWidth = skullRadius/ratios.SkullRadiusToMouthWidth
        let mouthHeight = skullRadius/ratios.SkullRadiusToMouthHeight
        
        let mouthRect = CGRect(x: skullCenter.x - mouthWidth/2, y: skullCenter.y+mouthOffset, width:mouthWidth, height: mouthHeight)
        
        let smileOffset = CGFloat(max(-1,min(mouthCurvature,1))) * mouthRect.height
        let start = CGPoint(x: mouthRect.minX,y: mouthRect.minY)
        let end = CGPoint(x: mouthRect.maxX, y: mouthRect.minY)
        
        let cp1 = CGPoint(x: mouthRect.minX + mouthRect.width / 3, y: mouthRect.minY + smileOffset)
        let cp2 = CGPoint(x: mouthRect.maxX - mouthRect.width / 3, y: mouthRect.minY + smileOffset)
        
        let path = UIBezierPath()
        path.move(to: start)
        path.addCurve(to: end, controlPoint1: cp1, controlPoint2: cp2)
        path.lineWidth = lineWidth
        
        return path
    }
    
    private func getEyeCenter(eye: Eye)->CGPoint{
        var eyeCenter = skullCenter
        let eyeOffset = skullRadius/ratios.SkullRadiusToEyeOffset
        switch eye{
        case .Left : eyeCenter.x-=eyeOffset
        case .Right : eyeCenter.x+=eyeOffset
        }
        eyeCenter.y-=eyeOffset
        return eyeCenter
    }
    
    
    override func draw(_ rect: CGRect) {
        strokeColor.setStroke()
        strokeColor.setFill()
        getPathForDrawingSkull(radius: skullRadius, center: skullCenter).stroke()
        getPathFordrawingEye(eye: .Left).stroke()
        getPathFordrawingEye(eye: .Right).stroke()
        getPathFordrawingEye(eye: .Left).fill()
        getPathFordrawingEye(eye: .Right).fill()
        getPathForDrawingMouth().stroke()
        
    }
    private struct ratios{
        static let SkullRadiusToEyeOffset: CGFloat = 3
        static let SkullRadiusToEyeRadius: CGFloat = 10
        static let SkullRadiusToMouthWidth: CGFloat = 1
        static let SkullRadiusToMouthHeight: CGFloat = 3
        static let SkullRadiusToMouthOffset: CGFloat = 2
        static let SkullRadiusToBrowOffset: CGFloat = 5
    }
    private enum Eye {
        case Left
        case Right
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        backgroundColor = UIColor.clear
        addSubview(message)
        addConstraintWithFormat(format: "H:|[v0]|", views: message)
        addConstraintWithFormat(format: "V:|-100-[v0]|", views: message)
    }
    
}
