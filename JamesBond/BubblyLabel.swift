//
//  BubblyLabel.swift
//  JamesBond
//
//  Created by Jatin Garg on 03/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

 class BubblyLabel: UILabel {

    var cornerRadius: CGFloat = 3
    var triangleHeight: CGFloat = 10
    var radius: CGFloat = 3
    var borderWidth: CGFloat = 1
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textColor = UIColor.black
        backgroundColor = UIColor.white
        self.layer.masksToBounds = true
    }
  
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        //masking path
        let path = UIBezierPath()
        path.move(to: CGPoint(x: triangleHeight, y: 0))
        _ = bounds.width - triangleHeight
        path.lineJoinStyle = .round
        path.addLine(to: CGPoint(x: bounds.width, y: 0))
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        path.addLine(to: CGPoint(x: triangleHeight, y: bounds.height))
        path.addLine(to: CGPoint(x: triangleHeight, y: bounds.height - bounds.height * 0.4))
        path.addLine(to: CGPoint(x: 0, y: bounds.height/2))
        path.addLine(to: CGPoint(x: triangleHeight, y: bounds.height * 0.4 ))
        path.close()
        
        //masking layer to clip lable according to path
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
        
        //layer to fill and stroke the path
        let bLayer = CAShapeLayer()
        bLayer.lineJoin = kCALineJoinRound
        bLayer.path = path.cgPath
        bLayer.fillColor = UIColor.clear.cgColor
        bLayer.strokeColor = UIColor.white.cgColor
        bLayer.lineWidth = borderWidth
        layer.addSublayer(bLayer)
        
    }
    
    override func drawText(in rect: CGRect) {
        let edgeInsets = UIEdgeInsetsMake(5, 15, 5, 5)
        super.drawText(in: UIEdgeInsetsInsetRect(rect,edgeInsets))
    }

}
