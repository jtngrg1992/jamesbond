//
//  UserDetailsViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 28/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.subviews.forEach { (v) in
            if let textField =  v as? FamTextField{
                if textField.isFirstResponder{
                    textField.resignFirstResponder()
                }
            }
        }
    }

   
}
