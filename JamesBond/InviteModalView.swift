//
//  InviteModalView.swift
//  JamesBond
//
//  Created by Jatin Garg on 01/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import MessageUI

protocol InviteModalViewDelegate{
    func modalClosed()
}
class InviteModalView: UIView, MFMessageComposeViewControllerDelegate {
    
    var delegate: InviteModalViewDelegate? = nil
    @IBOutlet weak var closeBtn: UIButton!
    @IBAction func whatsappIconPressed(_ sender: Any) {
        let whatsAppURL = NSURL.init(string: "whatsapp://send?text=Install%20FamBond%20now!")
        if UIApplication.shared.canOpenURL(whatsAppURL as! URL){
            UIApplication.shared.openURL(whatsAppURL as! URL)
        }
        else{
            self.window?.rootViewController?.present(Globals.sharedInstance.showMessage(message: "Whatsapp not found"), animated: true, completion: nil)
        }
    }
    @IBAction func messagedIconPressed(_ sender: Any) {
        if MFMessageComposeViewController.canSendText(){
            let controller = MFMessageComposeViewController()
            controller.body = "install Fambond now!"
            controller.messageComposeDelegate = self
            self.window?.rootViewController?.present(controller, animated: true, completion: nil)
        }
        else{
            self.window?.rootViewController?.present(Globals.sharedInstance.showMessage(message: "Unable to launch messaging app"), animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
       controller.dismiss(animated: true, completion: nil)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        delegate?.modalClosed()
    }
    
    func setUp()
    {
        let nib = Bundle.main.loadNibNamed("InviteModalView", owner: self, options: nil)?.first as! UIView
//        nib.bounds = self.bounds
        nib.backgroundColor = Globals.sharedInstance.secondaryColor
        nib.frame.size.width = self.frame.width
        nib.frame.size.height = self.frame.height
        self.addSubview(nib)
        
    }

}
