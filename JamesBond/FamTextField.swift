//
//  FamTextField.swift
//  JamesBond
//
//  Created by Jatin Garg on 27/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

class FamTextField: UITextField {
    
    var strokeColor: UIColor = Globals.sharedInstance.textFiledBoundaryColor
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    func setUp(){
        backgroundColor = UIColor.clear
        borderStyle = .none
        font = Globals.sharedInstance.btnfont
        textColor = UIColor.white
        translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.autoreverses = true
        animation.repeatCount = 5
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let startingPoint = CGPoint(x: bounds.origin.x, y: bounds.origin.y + bounds.height)
        path.move(to: startingPoint)
        strokeColor.setStroke()
        path.lineWidth = Globals.sharedInstance.btnLineWidth
        path.addLine(to: CGPoint(x: startingPoint.x + bounds.width, y: startingPoint.y))
        path.stroke()
    }

}
