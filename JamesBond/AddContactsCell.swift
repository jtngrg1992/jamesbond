//
//  AddContactsCell.swift
//  JamesBond
//
//  Created by Jatin Garg on 02/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit

class AddContactsCell: UICollectionViewCell {
    @IBOutlet weak var pictureContainer: UIView!
    @IBOutlet weak var displayPicture: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    
    override func awakeFromNib() {
        pictureContainer.layer.cornerRadius = pictureContainer.frame.width/2
        pictureContainer.layer.borderWidth = 2
        pictureContainer.layer.borderColor = Globals.sharedInstance.secondaryColor.cgColor
        pictureContainer.layer.masksToBounds = true
        pictureContainer.backgroundColor = UIColor.clear
        displayPicture.layer.cornerRadius = displayPicture.frame.width/2
        displayPicture.layer.masksToBounds = true
        addBtn.layer.borderColor = Globals.sharedInstance.secondaryColor.cgColor
        addBtn.layer.borderWidth = 2
        addBtn.titleLabel?.textColor = Globals.sharedInstance.secondaryColor
        addBtn.layer.cornerRadius = 3
    }
    
}
