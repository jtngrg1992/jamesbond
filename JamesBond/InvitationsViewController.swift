//
//  InvitationsViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 01/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import Contacts
import XMPPFramework

class InvitationsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, InviteModalViewDelegate, XMPPDelegate{

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedIndices = [Int]()
    var appDel: AppDelegate!
    var invitees = [Invitees]()
    var modalViewFrame : CGRect!
    var inviteModal: InviteModalView!
    var animator = UIDynamicAnimator()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewLayout.itemSize = CGSize(width: view.bounds.width, height: 100)
        navigationController?.navigationBar.isTranslucent  = false
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        navigationItem.titleView = createTitleLabel()
        appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.requestForAccess { (success) in
            if success{
                let contacts = self.appDel.retrieveContacts()
                let formatter = CNContactFormatter()
                contacts.forEach({ (contact) in
                    self.invitees.append(Invitees(num: contact.phoneNumbers.first?.value.stringValue as String?, name: formatter.string(from: contact) as String?))
                })
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
            }
            else{
                self.present(Globals.sharedInstance.showMessage(message: "Failed to obtain required permissions"), animated: true, completion: nil)
            }
        }
        animator = UIDynamicAnimator(referenceView: self.view)
    
        //XMPP
//        registerMe()
    }
    
    func registerMe(){
        if let delegate = UIApplication.shared.delegate as? AppDelegate{
            //UserDefaults.standard.set("9999464304@jlabs-win-pc", forKey: "userID")
            UserDefaults.standard.set("jatin2@localhost", forKey: "userID")
            UserDefaults.standard.set("admin", forKey: "userPassword")
            let connection = delegate.connect()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = createLeftBarButtonItem()
        navigationItem.rightBarButtonItem = createRightBarButtonItem()
        configureInviteModalView()
    }
    
    func createTitleLabel() -> UILabel
    {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: (navigationController?.navigationBar.frame.height)!))
        titleLabel.text = "Invite People"
        titleLabel.textColor = UIColor.white
        titleLabel.font = Globals.sharedInstance.navigationTitleFont
        return titleLabel
    }
    
    func createLeftBarButtonItem() -> UIBarButtonItem
    {
        let barButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(barBtnPressed))
        return barButton
    }
    
    func createRightBarButtonItem() -> UIBarButtonItem
    {
        let barBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "forward-arrow"), style: .plain, target: self, action: #selector(barBtnPressed))
        return barBtn
    }
    
    func configureInviteModalView()
    {
        inviteModal = InviteModalView(frame: CGRect(x: 20, y: self.view.frame.height/2 - 100, width: self.view.bounds.width - 40, height: 200))
        inviteModal.frame.size.height = 250
        inviteModal.backgroundColor = UIColor.black
        inviteModal.layer.cornerRadius = 5
        inviteModal.layer.masksToBounds = true
        modalViewFrame = inviteModal.frame
        inviteModal.frame.origin.y = self.view.frame.height
        inviteModal.delegate = self
        view.addSubview(inviteModal)
        
    }
    func barBtnPressed(btn: UIBarButtonItem)
    {
        performSegue(withIdentifier: "addContacts", sender: self)
    }
    func modalClosed()
    {
        let snap = UISnapBehavior(item: inviteModal, snapTo: CGPoint(x: 0, y: self.view.frame.height + inviteModal.frame.height))
        animator.removeAllBehaviors()
        animator.addBehavior(snap)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return invitees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! InvitationsCell
        cell.contactLabel.text = invitees[indexPath.row].name
        if selectedIndices.contains(indexPath.row){
            cell.checked = true
        }
        else{
            cell.checked = false
        }
        
        //adding alternating bg color
        switch indexPath.row % 2 {
        case 0:
            cell.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        default:
            cell.backgroundColor = Globals.sharedInstance.backgroundColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? InvitationsCell
        cell?.checked = !(cell?.checked)!
        if selectedIndices.contains(indexPath.row){
            selectedIndices.remove(at: selectedIndices.index(of: indexPath.row)!)
        }
        else{
            selectedIndices.append(indexPath.row)
        }
        
       
        let snap = UISnapBehavior(item: inviteModal, snapTo: self.view.center)
        animator.removeAllBehaviors()
        animator.addBehavior(snap)
        
    }
    
    
    

}
