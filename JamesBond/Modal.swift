//
//  Modal.swift
//  JamesBond
//
//  Created by Jatin Garg on 24/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import  UIKit
import FMDB

class Bond: NSObject{
    var id: Int? = nil
    var title: String? = nil
    var createdOn: Date? = nil
}

class Message: NSObject{
    var id: Int? = nil
    var text: String? = nil
    var date: Date? = nil
}

class Contact: NSObject{
    var id: Int? nil
    var name: String? = nil
    var phone: String? = nil
}

class Utility{
    class func getPath(fileName: String) -> String{
        let docURl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = docURl.appendingPathComponent(fileName)
        return fileURL.path
    }
    
    class func copyFile(fileName: NSString){
        let dbPath: String = getPath(fileName: fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath){
            let docURL = Bundle.main.resourceURL
            let fromPath = docURL?.appendingPathComponent(fileName as String)
            var error: NSError?
            do{
                try fileManager.copyItem(atPath: (fromPath?.path)!, toPath: dbPath)
            }
            catch let err{
                error = err as NSError
            }
            
            let alert: UIAlertView = UIAlertView()
            if error != nil{
                alert.title = "Error Occured"
                alert.message = error?.localizedDescription
            } else{
                alert.title = "Successfull Copy"
                alert.message = "DB copied Successfully"
            }
            
            alert.delegate = self
            alert.addButton(withTitle: "Ok")
            alert.show()
        }
    }
    
    class func invokeAlertMethod(title: NSString,body: NSString,delegate: AnyObject?){
        let alert = UIAlertView()
        alert.message = body as String
        alert.title = title as String
        alert.delegate = delegate
        alert.addButton(withTitle: "OK")
        alert.show()
        
    }
    
    class func getDBInstance() -> FMDatabase?{
        let dbPath = getPath(fileName: "FamBond.sqlite")
        let db = FMDatabase(path: dbPath)
        return db
        
    }
    class func insertToContacts(name: String, phone: String){
        let db = getDBInstance()
        if db == nil{
            print("Error:\(db?.lastErrorMessage())")
        }
        if (db?.open())!{
            let sql = "Insert into Contact(name,phone) values(?,?)"
            if (db?.executeUpdate(sql, withArgumentsIn: [name,phone]))!{
                print("Insert Successful")
            }else{
                print("Error: \(db?.lastErrorMessage())")
            }
        }
        db?.close()
    }
    
    class func getBondData() -> [Bond]?{
        let db = getDBInstance()
        if db == nil{
            print("Error: \(db?.lastErrorMessage())")
        }
        if (db?.open())!{
            let sql = "Select * from Bond"
            do{
                var bonds = [Bond]()
                let results = try db?.executeQuery(sql, values: nil)
                while (results?.next())!{
                    let bond = Bond()
                    guard let title = results?.string(forColumn: "title") else {print("no bond title to guard"); db?.close();return nil}
                    guard let createdOn = results?.date(forColumn: "createOn") else{print("no bond date to guard"); db?.close();return nil}
                    bond.title = title
                    bond.createdOn = createdOn
                    bonds.append(bond)
                }
                db?.close()
                return bonds
            }catch let err{
                print("Error: \(err)")
            }
            
        }
        db?.close()
        return nil
    }
    
    class func getContactData() -> [Contact]?{
        let db = getDBInstance()
        if db == nil{
            print("Error:\(db?.lastErrorMessage())")
        }
        if (db?.open())!{
            let sql = "Select * from Contact"
            do{
                var contacts = [Contact]()
                let results = try db?.executeQuery(sql, values: nil)
                while (results?.next())!{
                    let contact = Contact()
                    guard let name = results?.string(forColumn: "name")else{print("No contact name to guard"); db?.close();return nil}
                    guard let phone = results?.string(forColumn: "phone")else{print("No contact phone to guard"); db?.close();return nil}
                    contact.name = name
                    contact.phone = phone
                    contacts.append(contact)
                }
                db?.close()
                return contacts
            }catch let err{
                print("Error: \(err)")
            }
        }
        return nil
    }
    
    class func saveBond(title: String, members: [Contact]) {
        let db = getDBInstance()
        var sql = "Insert into Bond(title,createdOn) values(?,?)"
        if (db?.executeUpdate(sql, withArgumentsIn: [title,Date()]))!{
                members.forEach({ (member) in
                    sql = "insert into isMemberOf(bondID,contactID) values()"
                })
        }else{
            print("Error: \(db?.lastErrorMessage())")
        }
    }
}

