//
//  BondsViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 02/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import BEMCheckBox
import CADRACSwippableCell
import ReactiveCocoa
import XMPPFramework
extension UIView{
    func addConstraintWithFormat(format: String, views: UIView...){
        var viewDict = [String: UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            viewDict[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
    }
    
}


class BondsViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, XMPPDelegate{

    @IBOutlet weak var addBondsBtn: UIButton!
    @IBOutlet weak var bondButton: UIButton!
    @IBOutlet weak var createBondsLabel: BubblyLabel!
    @IBOutlet weak var noBondAssetContainer: UIView!
    @IBOutlet weak var nobondsAssetTrailingConstraint: NSLayoutConstraint!
    var revealVC: SWRevealViewController!
    var moreOptionsShowing: Bool = false
    var noBondAnimationShowing: Bool = true
    var dimView: UIView!
    var noBondsAssetsContainerCenter: CGPoint!
    var animator = UIDynamicAnimator()
    var chatBubble:UIImageView = {
        let image = UIImage(named: "chat")?.resizableImage(withCapInsets: UIEdgeInsetsMake(8,8,8,8))
        var imgView = UIImageView()
        imgView.image = image
        return imgView
    }()
    
    let newCallBtn: UIButton = {
       let btn = UIButton()
        btn.backgroundColor = UIColor.black
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 25
        btn.setImage(UIImage(named: "call-btn"), for: .normal)
        return btn
    }()
    
    var newCallBtnConstraints = [NSLayoutConstraint]()
    var newPeopleBtnConstraints = [NSLayoutConstraint]()
    let newPeopleBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.black
        btn.setImage(UIImage(named: "user-btn"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 25
        return btn
    }()
    
    var emptyBack: EmptyBondsBackground = {
        let v = EmptyBondsBackground()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var collectionView: UICollectionView = {
        let c = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        c.translatesAutoresizingMaskIntoConstraints = false
        c.backgroundColor = UIColor.clear
        c.alwaysBounceVertical = true
        return c
    }()
    
    var bondsToDisplay = [Bond]()
    var pop: Bool = false
    var xmppDelegate: XMPPDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        animator = UIDynamicAnimator(referenceView: self.view)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        navigationController?.navigationBar.tintColor = UIColor.white
        Globals.sharedInstance.configureTitleView(navigationController: navigationController, title: "Bonds", subTitle: "", navigationItem: navigationItem)
        configureLeftBarButton()
        configureRightBarButton()
        configureBondsButton()
        configureMyTab()
        bondButton.layer.cornerRadius = bondButton.frame.width/2
        bondButton.backgroundColor = UIColor.white
        noBondsAssetsContainerCenter = noBondAssetContainer.center
        nobondsAssetTrailingConstraint.constant -= view.frame.width
        loadBonds()
        //adding targets
        newCallBtn.addTarget(self, action: #selector(newCallBtnPressed), for: .touchUpInside)
        newPeopleBtn.addTarget(self, action: #selector(newPeopleBtnPressed), for: .touchUpInside)
        //creating more option views
        view.insertSubview(newCallBtn, belowSubview: addBondsBtn)
        view.insertSubview(newPeopleBtn, belowSubview: addBondsBtn)
        configureEmptyBackgroudView()
        configureCollectionView()
        configureNewCallBtn()
        configureAddPeopleBtn()
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if bondsToDisplay.count == 0{
                configureNoBondsAnimation()
        }
        
        revealVC = tabBarController?.revealViewController()
        
}
    
    func recievedMessage(message: [String : Any]) {
        print("Recieved MEsAGE!!")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
       animateStuff()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadBonds()
        if moreOptionsShowing{
            animateStuff()
        }
    }
    
    func loadBonds(){
        bondsToDisplay = [Bond]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bond")
        let sortDesc = NSSortDescriptor(key: "createdOn", ascending: false)
        fetchRequest.sortDescriptors = [sortDesc]
        let context = DBManager.getContext()
        do{
            try bondsToDisplay = context.fetch(fetchRequest) as! [Bond]
        }catch{
            print("Error Fetching : \(error)")
        }
        
    }
    func animateStuff(){
        if noBondAnimationShowing{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                self.dimView?.alpha = 0
                self.noBondAssetContainer?.alpha = 0
            })
            noBondAnimationShowing = false
        }
        
        if moreOptionsShowing{
            addBondsBtn.setImage(nil, for: .normal)
            addBondsBtn.setTitle("+", for: .normal)
            var verticalConstraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerY, relatedBy: .equal, toItem: newCallBtn, attribute: .centerY, multiplier: 1, constant: 0)
            view.removeConstraint(newCallBtnConstraints[1])
            newCallBtnConstraints.insert(verticalConstraint, at: 1)
            view.addConstraint(verticalConstraint)
            
            verticalConstraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerY, relatedBy: .equal, toItem: newPeopleBtn, attribute: .centerY, multiplier: 1, constant: 0)
            view.removeConstraint(newPeopleBtnConstraints[1])
            newPeopleBtnConstraints.insert(verticalConstraint, at: 1)
            view.addConstraint(verticalConstraint)
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                self.view.layoutIfNeeded()
            }, completion: {(completed) in
                //animate text labels here in future??
                self.newCallBtn.isHidden = true
                self.newPeopleBtn.isHidden = true
                
            })
            moreOptionsShowing = false
        }

    }
    
    func configureCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: view.frame.width, height: 100)
        layout.sectionInset.top = 2
        layout.minimumLineSpacing = 2
        collectionView.collectionViewLayout = layout
        view.insertSubview(collectionView, aboveSubview: emptyBack)
        if bondsToDisplay.count != 0{
            collectionView.frame = view.frame
            view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
            view.addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
            
        }
        else{
            collectionView.frame = CGRect.zero
        }
        collectionView.backgroundColor = view.backgroundColor
        collectionView.register(BondsCell.self, forCellWithReuseIdentifier: "cell")
//        collectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(collectionViewTapped)))
    }
    
    func collectionViewTapped(gesture: UITapGestureRecognizer){
        animateStuff()
    }
    func configureEmptyBackgroudView(){
        view.insertSubview(emptyBack, belowSubview: addBondsBtn)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: emptyBack)
        view.addConstraintWithFormat(format: "V:|[v0(300)]", views: emptyBack)
    }
    
    func newCallBtnPressed(sender: UIButton){
        print("New Call")
    }
    
    func newPeopleBtnPressed(sender: UIButton){
        
    }
    func configureAddPeopleBtn(){
        let horizontalContraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerX, relatedBy: .equal, toItem: newPeopleBtn, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalContstraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerY, relatedBy: .equal, toItem: newPeopleBtn, attribute: .centerY, multiplier: 1, constant: 0)
        let widthContraint = NSLayoutConstraint(item: newPeopleBtn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        let heightContraint = NSLayoutConstraint(item: newPeopleBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        newPeopleBtnConstraints.append(horizontalContraint)
        newPeopleBtnConstraints.append(verticalContstraint)
        newPeopleBtnConstraints.append(widthContraint)
        newPeopleBtnConstraints.append(heightContraint)
        view.insertSubview(newPeopleBtn, aboveSubview: collectionView)
        view.addConstraints(newPeopleBtnConstraints)
        newPeopleBtn.isHidden = true
    }
    func configureNewCallBtn(){
        let horizontalContraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerX, relatedBy: .equal, toItem: newCallBtn, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalContstraint = NSLayoutConstraint(item: addBondsBtn, attribute: .centerY, relatedBy: .equal, toItem: newCallBtn, attribute: .centerY, multiplier: 1, constant: 10)
        let widthContraint = NSLayoutConstraint(item: newCallBtn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        let heightContraint = NSLayoutConstraint(item: newCallBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        newCallBtnConstraints.append(horizontalContraint)
        newCallBtnConstraints.append(verticalContstraint)
        newCallBtnConstraints.append(widthContraint)
        newCallBtnConstraints.append(heightContraint)
        view.insertSubview(newCallBtn, aboveSubview: collectionView)
        view.addConstraints(newCallBtnConstraints)
        newCallBtn.isHidden = true
    }
    
    func configureLeftBarButton()
    {
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-button"), style: .plain, target: revealVC, action: #selector(revealVC.revealToggle(_:)))
        
        navigationItem.leftBarButtonItem = btn
    }
    
    func configureRightBarButton()
    {
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: #selector(barBtnPressed(btn:)))
        navigationItem.rightBarButtonItem = btn
    }
    
    func configureMyTab()
    {
        tabBarController?.tabBar.isTranslucent = false
        tabBarController?.tabBar.barTintColor = Globals.sharedInstance.backgroundColor
        tabBarController?.tabBar.tintColor = UIColor.white
        let image = UIImage(named: "bond-tab")?.withRenderingMode(.alwaysOriginal)
        
        let tabBarItem = UITabBarItem(title: "Bonds", image: image, selectedImage: image)
        self.tabBarItem = tabBarItem
    }
    
    func configureBondsButton()
    {
        addBondsBtn.layer.masksToBounds = true
        addBondsBtn.layer.cornerRadius = addBondsBtn.bounds.width/2
        addBondsBtn.backgroundColor = Globals.sharedInstance.secondaryColor
    }
    
    func configureNoBondsAnimation()
    {
        //insert darker overlay
        dimView = UIView(frame: view.frame)
        dimView.frame.origin.y -= (navigationController?.navigationBar.frame.height)! + 20
        dimView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.insertSubview(dimView, belowSubview: addBondsBtn)
        dimView.frame.origin.y += view.frame.height
        //slide out required controlls
        self.nobondsAssetTrailingConstraint.constant += self.view.frame.width
        UIView.animate(withDuration: 0.4, animations:{() -> Void in
           self.dimView.frame.origin.y -= self.view.frame.height
            self.view.layoutIfNeeded()
        })
    }
    @IBAction func addBondsBtnTouchDown(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.dimView?.alpha = 0
            self.noBondAssetContainer?.alpha = 0
        })
        if !moreOptionsShowing{
            addBondsBtn.setTitle("", for: .normal)
            addBondsBtn.setImage(UIImage(named: "bond-tab")?.withRenderingMode(.alwaysTemplate), for: .normal)
            addBondsBtn.imageView?.tintColor = UIColor.white
            newCallBtn.isHidden = false
            view.removeConstraint(newCallBtnConstraints[1])
            var verticalConstraint = NSLayoutConstraint(item: addBondsBtn, attribute: .top, relatedBy: .equal, toItem: newCallBtn, attribute: .bottom, multiplier: 1, constant: 10)
            newCallBtnConstraints.remove(at: 1)
            newCallBtnConstraints.insert(verticalConstraint, at: 1)
            view.addConstraint(verticalConstraint)
            
            newPeopleBtn.isHidden = false
            view.removeConstraint(newPeopleBtnConstraints[1])
            verticalConstraint = NSLayoutConstraint(item: newCallBtn, attribute: .top, relatedBy: .equal, toItem: newPeopleBtn, attribute: .bottom, multiplier: 1, constant: 10)
            newPeopleBtnConstraints.remove(at: 1)
            newPeopleBtnConstraints.insert(verticalConstraint, at: 1)
            view.addConstraint(verticalConstraint)
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                self.view.layoutIfNeeded()
            }, completion: {(completed) in
                //animate text labels here in future??
                
            })
            moreOptionsShowing = true
        }
        else{
            let newBondsVC = NewBondsViewController()
            navigationController?.pushViewController(newBondsVC, animated: true)
            animateStuff()
            
        }
        
    }
    
    //MARK :- CollectionView Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bondsToDisplay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BondsCell
        cell.backgroundColor = Globals.sharedInstance.backgroundColor
        let bottomView = BottomView()
        let size = CGSize(width: collectionView.bounds.width/2, height: cell.bounds.height)
        bottomView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        cell.allowedDirection = .left
        cell.revealView = bottomView
        cell.revealViewSignal.filter { (isRevealed) -> Bool in
            return isRevealed as! Bool
        }.subscribeNext { (id) in
            for (_,value) in collectionView.visibleCells.enumerated(){
                let otherCell = value as! BondsCell
                if otherCell != cell{
                    otherCell.hideRevealView(animated: true)
                }
            }
        }
        // the real stuff
        if let title = bondsToDisplay[indexPath.item].title{
            cell.nameLabel.text = title
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        animateStuff()
        if let title = bondsToDisplay[indexPath.item].title{
            let chatLog = ChatLogViewController()
            chatLog.bondTitle = title
            chatLog.hidesBottomBarWhenPushed = true
            chatLog.pan = nil
            chatLog.hidesBackButton = false
            navigationController?.pushViewController(chatLog, animated: true)
        }
    }
    
    @IBAction func addBondsBtnTouchup(_ sender: Any) {
        addBondsBtn.alpha = 1.0

    }
   
    func barBtnPressed(btn: UIBarButtonItem)
    {
        
    }
    
    func configureMoreOptionButtons()
    {
        
    }
   
}

class BondsCell: CADRACSwippableCell{
    let nameLabel: FamLabel={
        let l = FamLabel()
        l.text = "Contact Name"
        l.font = UIFont(name: "AvenirNext-Bold", size: 17)
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let profilePic: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "contact-picture")
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 25
        image.layer.masksToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let profilePicBorderView: UIView = {
        let v = UIView()
        v.layer.borderWidth = 2
        v.layer.borderColor = UIColor(white: 0.65, alpha: 1).cgColor
        v.layer.cornerRadius = 30
        v.layer.masksToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let frontLabel: UILabel = {
        let c = UILabel()
        c.translatesAutoresizingMaskIntoConstraints = false
        c.font = UIFont(name: "Avenir Next", size: 15)
        c.textColor = UIColor.white
        c.text = ">"
        return c
    }()
    
    let timeLabel: UILabel = {
        let c = UILabel()
        c.translatesAutoresizingMaskIntoConstraints = false
        c.font = UIFont(name: "Avenir Next", size: 12)
        c.textColor = UIColor.white
        c.text = "7:58 PM"
        return c
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        contentView.addSubview(nameLabel)
        contentView.addSubview(profilePicBorderView)
        profilePicBorderView.addSubview(profilePic)
        contentView.addSubview(frontLabel)
        contentView.addSubview(timeLabel)
        addConstraintWithFormat(format: "H:|-10-[v0(60)]-10-[v1][v2][v3]-5-|", views: profilePicBorderView,nameLabel,timeLabel,frontLabel)
        addConstraintWithFormat(format: "V:|-10-[v0]", views: timeLabel)
        addConstraintWithFormat(format: "V:[v0(60)]", views: profilePicBorderView)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: profilePicBorderView, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: nameLabel, attribute: .centerY, multiplier: 1, constant: 0))
        profilePicBorderView.addConstraintWithFormat(format: "H:[v0(50)]", views: profilePic)
        profilePicBorderView.addConstraintWithFormat(format: "V:[v0(50)]", views: profilePic)
        addConstraint(NSLayoutConstraint(item: profilePicBorderView, attribute: .centerX, relatedBy: .equal, toItem: profilePic, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: profilePicBorderView, attribute: .centerY, relatedBy: .equal, toItem: profilePic, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraintWithFormat(format: "V:[v0(30)]", views: frontLabel)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: frontLabel, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
}

class BottomView: UIView{
    var removeBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "remove"), for: .normal)
        return b
}()
    
    var addMembtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "addMember"), for: .normal)
        return b
    }()
    
    var muteBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "mute"), for: .normal)
        return b
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        backgroundColor = Globals.sharedInstance.secondaryColor
        addSubview(removeBtn)
        addSubview(addMembtn)
        addSubview(muteBtn)
        addConstraintWithFormat(format: "H:[v0(30)]-20-[v1(30)]-20-[v2(30)]-10-|", views: removeBtn,addMembtn,muteBtn)
        addConstraintWithFormat(format: "V:[v0(30)]", views: removeBtn)
        addConstraintWithFormat(format: "V:[v0(30)]", views: addMembtn)
        addConstraintWithFormat(format: "V:[v0(30)]", views: muteBtn)
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: removeBtn, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: addMembtn, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: muteBtn, attribute: .centerY, multiplier: 1, constant: 0))
        
        removeBtn.addTarget(self, action: #selector(removed), for: .touchUpInside)
        addMembtn.addTarget(self, action: #selector(added), for: .touchUpInside)
        
    }
    
    func removed(sender: UIButton){
        sender.isHighlighted = true
        print("remvd")
    }
    
    func added(sender: UIButton){
        
    }
}
