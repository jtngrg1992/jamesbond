//
//  ViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 27/10/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import CoreTelephony

class EntryViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource{
    
    let countryCodes:[String] = ["IN - 91", "AF - 93", "BA - 242", "AG - 54","AZ - 994", "BA - 880"]
    var tableViewToggled: Bool = false
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countryCode: FamTextField!
    @IBOutlet weak var phoneNumber: FamTextField!
    
    override func viewDidLoad() {
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        phoneNumber.keyboardType = .numberPad
        phoneNumber.delegate = self
        countryCode.delegate = self
        countryCode.textColor = UIColor.white
        countryCode.leftView = createDropDownBtn(frame: countryCode.bounds)
        countryCode.leftViewMode = .always
        countryCode.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewPressed)))
        phoneNumber.textColor = UIColor.white
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        tableView.alpha = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == countryCode{
            return false
        }
        else{
            return true
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if countryCode.isFirstResponder{
            countryCode.resignFirstResponder()
        }
        else{
            phoneNumber.resignFirstResponder()

        }
    }
    
   
    func btnTapped(btn: UIButton)
    {
        var alphavalue: CGFloat = 0
        if !tableViewToggled{
            alphavalue = 1
        }
        else{
            alphavalue = 0
        }
        tableViewToggled = !tableViewToggled
        
        UIView.animate(withDuration: 0.4, animations: {
            self.tableView.alpha = alphavalue
        })
    }
    
    func viewPressed(rec: UITapGestureRecognizer)
    {
        print("View Presssed")
    }
    func getCountryCode()
    {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider as? String
        print(carrier)
        
    }
    
    func createDropDownBtn(frame: CGRect) -> UIButton
    {
        let dropBtn = UIButton()
        dropBtn.frame = frame.insetBy(dx: 15, dy: 8)
        dropBtn.setImage(#imageLiteral(resourceName: "down-arrow"), for: .normal)
        dropBtn.setTitle("IN", for: .normal)
        dropBtn.setTitleColor(Globals.sharedInstance.secondaryColor, for: .normal)
        dropBtn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        dropBtn.addTarget(self, action: #selector(btnTapped(btn:)), for: .touchUpInside)
        return dropBtn
    }
    
    
}

extension EntryViewController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryCodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let label = cell?.viewWithTag(1) as! UILabel
        label.text = countryCodes[indexPath.row]
        return cell!
    }
}

