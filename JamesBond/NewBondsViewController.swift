//
//  NewBondsViewController.swift
//  JamesBond
//
//  Created by Jatin Garg on 15/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import BEMCheckBox
import FMDB
import CoreData

class FamLabel: UILabel{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        font = Globals.sharedInstance.btnfont
        textColor = UIColor.white
        translatesAutoresizingMaskIntoConstraints = false
    }
}
class NewBondsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var collectionView: UICollectionView! = {
        var layout = UICollectionViewFlowLayout()
        let coll = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        coll.translatesAutoresizingMaskIntoConstraints = false
        coll.register(NewBondCell.self, forCellWithReuseIdentifier: "cell")
        coll.alwaysBounceVertical = true
        return coll
    }()
    
    var forwardBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 35
        btn.backgroundColor = Globals.sharedInstance.secondaryColor
        btn.setImage(UIImage(named: "forward-arrow-1"), for: .normal)
        return btn
    }()
    
    var contacts: [Contact]!{
        didSet{
            collectionView.reloadData()
        }
    }
    
    var selectedContacts: [Contact]! = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        Globals.sharedInstance.configureTitleView(navigationController: navigationController, title: "Add Bond", subTitle: "Add People To Your Bond", navigationItem: navigationItem)
        configureSearchbtn()
        configureCollectionView()
        configureForwardBtn()
        getSavedContacts()
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    func getSavedContacts(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        do{
            contacts = try DBManager.getContext().fetch(fetchRequest) as! [Contact]
        }catch {
            print("Error fetching contacts: \(error)")
        }
    }
    
    //MARK :- collection view delegate methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NewBondCell
        if let name = contacts[indexPath.item].name{
            cell.nameLabel.text = name
        }
        
        if indexPath.item == 0{
            cell.checkBox.on = true
            selectedContacts.append(contacts[0])
        }
        
        
        cell.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! NewBondCell
        if selectedContacts.contains(contacts[indexPath.item]){
            cell.checkBox.setOn(false, animated: true)
            selectedContacts.remove(at: selectedContacts.index(of: contacts[indexPath.item])!)
        }
        else{
            cell.checkBox.setOn(true, animated: true)
            selectedContacts.append(contacts[indexPath.item])
        }
        
    }
    
    //MARK -: collection view delegate methods end
    
    func configureForwardBtn(){
        view.addSubview(forwardBtn)
        view.addConstraintWithFormat(format: "H:[v0(70)]-30-|", views: forwardBtn)
        view.addConstraintWithFormat(format: "V:[v0(70)]|", views: forwardBtn)
        forwardBtn.addTarget(self, action: #selector(forwardBtnClicked), for: .touchUpInside)
        
    }
    
    func forwardBtnClicked(sender: UIButton){
        let v = NewBondsAddSubjectViewController()
        v.contacts = contacts
        v.selectedContacts = selectedContacts
        navigationController?.pushViewController(v, animated: true)
    }
    func configureCollectionView(){
        collectionView.frame = view.frame
        collectionView.dataSource = self
        collectionView.delegate = self
        
        view.addSubview(collectionView)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        collectionView.backgroundColor = UIColor.clear
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collectionView.frame.width, height: 100)
        layout.minimumLineSpacing = 2
        collectionView.collectionViewLayout = layout
    }
    
    
    
    func configureSearchbtn(){
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem  = search
    }

}

class NewBondCell: UICollectionViewCell{
    let nameLabel: FamLabel={
        let l = FamLabel()
        l.text = "Contact Name"
        l.font = UIFont(name: "AvenirNext-Bold", size: 17)
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let profilePic: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "contact-picture")
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 25
        image.layer.masksToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let profilePicBorderView: UIView = {
        let v = UIView()
        v.layer.borderWidth = 2
        v.layer.borderColor = UIColor(white: 0.65, alpha: 1).cgColor
        v.layer.cornerRadius = 30
        v.layer.masksToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let checkBox: BEMCheckBox = {
        let c = BEMCheckBox()
        c.animationDuration = 0.3
        c.onAnimationType = .bounce
        c.onCheckColor = Globals.sharedInstance.secondaryColor
        c.translatesAutoresizingMaskIntoConstraints = false
        return c
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        addSubview(nameLabel)
        addSubview(profilePicBorderView)
        profilePicBorderView.addSubview(profilePic)
        addSubview(checkBox)
        
        addConstraintWithFormat(format: "H:|-10-[v0(60)]-10-[v1][v2(30)]-20-|", views: profilePicBorderView,nameLabel,checkBox)
        addConstraintWithFormat(format: "V:[v0(60)]", views: profilePicBorderView)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: profilePicBorderView, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: nameLabel, attribute: .centerY, multiplier: 1, constant: 0))
        profilePicBorderView.addConstraintWithFormat(format: "H:[v0(50)]", views: profilePic)
        profilePicBorderView.addConstraintWithFormat(format: "V:[v0(50)]", views: profilePic)
        addConstraint(NSLayoutConstraint(item: profilePicBorderView, attribute: .centerX, relatedBy: .equal, toItem: profilePic, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: profilePicBorderView, attribute: .centerY, relatedBy: .equal, toItem: profilePic, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraintWithFormat(format: "V:[v0(30)]", views: checkBox)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: checkBox, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
