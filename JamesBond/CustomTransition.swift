//
//  CustomTransition.swift
//  JamesBond
//
//  Created by Jatin Garg on 19/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class CustomTransition: NSObject, UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVC = transitionContext.viewController(forKey: .from)!
        let toVC = transitionContext.viewController(forKey: .to)!
        let containerView = transitionContext.containerView
        let bounds = UIScreen.main.bounds
        containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        let topRightCorner = CGPoint(x: bounds.width, y: 0)
        let finalFrame = CGRect(origin: topRightCorner, size: bounds.size)
        
//        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: .curveLinear, animations: {
//            fromVC.view.alpha = 0.5
//            toVC.view.frame = finalFrameForVC
//        }, completion: {
//            finished in
//            transitionContext.completeTransition(true)
//            fromVC.view.alpha = 1.0
//        })
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromVC.view.frame = finalFrame
        }) { (complete) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
