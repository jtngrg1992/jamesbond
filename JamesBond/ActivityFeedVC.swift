//
//  ActivityFeedVC.swift
//  JamesBond
//
//  Created by Jatin Garg on 22/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
class ActivityFeedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 2
        layout.sectionInset.top = 10
        layout.itemSize = CGSize(width: self.view.bounds.width, height: 400)
        var c = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        c.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        c.delegate = self
        c.dataSource = self
        c.translatesAutoresizingMaskIntoConstraints = false
        c.register(ActivityFeedCell.self, forCellWithReuseIdentifier: "cell")
        return c
    }()
    
    var revealVC: SWRevealViewController!
    
    override func viewDidLoad() {
        setupNavigationBar()
        setupCollectionView()
    }
    
    func setupCollectionView(){
        view.addSubview(collectionView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
    }
    
    func setupNavigationBar(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Globals.sharedInstance.backgroundColor
        navigationController?.navigationBar.tintColor = UIColor.white
        let label = FamLabel(frame: CGRect(x: 0, y: 0, width: 60, height: 70))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "ACTIVITY FEED"
        navigationItem.titleView = label
        
        var btn = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-button"), style: .plain, target: revealVC, action: #selector(revealVC.revealToggle(_:)))
        navigationItem.leftBarButtonItem = btn
        btn = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = btn

        
        
    }
}

//MARK: - Collection View Delegate & Data Source Methods

extension ActivityFeedViewController{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ActivityFeedCell
        cell.backgroundColor = Globals.sharedInstance.backgroundColor
        return cell
    }
}

//MARK :- Supplementary Classes

class ActivityFeedCell: UICollectionViewCell{
    var containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        return v
    }()
    
    var commenntsBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.titleLabel?.font = UIFont(name: "Avenir Next", size: 12)
        b.titleLabel?.textColor = UIColor.white
        b.setTitle("Comments(12)", for: .normal)
//        b.backgroundColor = UIColor.black
        return b
    }() 
    
    var titleLabel: FamLabel = {
        let l = FamLabel()
        l.text = "Poll : Lunch Buddies"
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(titleLabel)
        addSubview(containerView)
        addSubview(commenntsBtn)
        
        addConstraintWithFormat(format: "H:|-20-[v0]", views: titleLabel)
        addConstraintWithFormat(format: "H:[v0]-20-|", views: commenntsBtn)
        addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: containerView)
        addConstraintWithFormat(format: "V:|-5-[v0(30)]-2-[v1]-2-[v2(30)]|", views: titleLabel,containerView,commenntsBtn)
    }
    
    
}
