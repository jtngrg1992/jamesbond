//
//  DBManager.swift
//  JamesBond
//
//  Created by Jatin Garg on 28/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import CoreData

class DBManager: NSObject{
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "JamesBondCoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    class func getContext() -> NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    // MARK: - Core Data Saving support
    
    class func saveContext () {
        let context = getContext()
        if context.hasChanges {
            do {
                try context.save()
                print("Success")
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
