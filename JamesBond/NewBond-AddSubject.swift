//
//  NewBond-AddSubject.swift
//  JamesBond
//
//  Created by Jatin Garg on 18/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class NewBondsAddSubjectViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    var customisationView: UIView = {
        let v = UIView()
        v.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    
    var tickBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.layer.cornerRadius = 35
        b.backgroundColor = Globals.sharedInstance.secondaryColor
        b.setImage(UIImage(named:"checked"), for: .normal)
        return b
    }()
    
    var profileImageView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.layer.cornerRadius = 35
        i.layer.masksToBounds = false
        i.image = UIImage(named: "photo-camera")
        i.contentMode = .scaleAspectFit
        i.backgroundColor = UIColor.white
        i.clipsToBounds = true
        return i
    }()
    
    var bondTitleView: FamTextField = {
        let t = FamTextField()
        t.attributedPlaceholder = Globals.sharedInstance.getAttributedPlaceholder(text: "Bond Subject",size: 17)
        return t
    }()
    
    var indicatorLabel: FamLabel = {
        let l = FamLabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "2/50"
        return l
    }()
    
    var collectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        let coll = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        coll.translatesAutoresizingMaskIntoConstraints = false
        coll.register(NewBondCell.self, forCellWithReuseIdentifier: "cell")
        coll.alwaysBounceVertical = true
        coll.backgroundColor = Globals.sharedInstance.backgroundColor
        return coll
    }()
    
    var contacts: [Contact]!
    var selectedContacts: [Contact]!
    
    override func viewDidLoad() {
        view.backgroundColor = Globals.sharedInstance.backgroundColor
        Globals.sharedInstance.configureTitleView(navigationController: navigationController, title: "Add Bond", subTitle: "Add Bond Subject", navigationItem: navigationItem)
        configureCustomisationView()
        configureCollectionView()
        indicatorLabel.text = "\(selectedContacts.count)/\(contacts.count)"
    }
    
    override func viewDidLayoutSubviews() {
        configureCheckBtn()
    }
    
    
    
    func configureCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: view.frame.width, height: 100)
        layout.minimumLineSpacing = 2
        layout.sectionInset.top = 5
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    func configureCustomisationView(){
        view.addSubview(customisationView)
        view.addSubview(collectionView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: customisationView)
        view.addConstraintWithFormat(format: "V:|[v0(>=150)]-300-|", views: customisationView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        view.addConstraint(NSLayoutConstraint(item: customisationView, attribute: .bottom, relatedBy: .equal, toItem: collectionView, attribute: .top, multiplier: 1, constant: 2))
        view.addConstraintWithFormat(format: "V:[v0]|", views: collectionView)
        configureCollectionView()
        
        //configuring subviews
        customisationView.addSubview(profileImageView)
        customisationView.addSubview(bondTitleView)
        customisationView.addSubview(indicatorLabel)
        customisationView.addConstraintWithFormat(format: "H:|-10-[v0(70)]-10-[v1]-20-|", views: profileImageView,bondTitleView)
        customisationView.addConstraintWithFormat(format: "V:[v0(70)]", views: profileImageView)
        customisationView.addConstraint(NSLayoutConstraint(item: customisationView, attribute: .centerY, relatedBy: .equal, toItem: profileImageView, attribute: .centerY, multiplier: 1, constant: 0))
        customisationView.addConstraint(NSLayoutConstraint(item: customisationView, attribute: .centerY, relatedBy: .equal, toItem: bondTitleView, attribute: .centerY, multiplier: 1, constant: 0))
        customisationView.addConstraintWithFormat(format: "V:[v0]-10-|", views: indicatorLabel)
        customisationView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .centerX, relatedBy: .equal, toItem: indicatorLabel, attribute: .centerX, multiplier: 1, constant: 0))
        
    }
    
    func configureCheckBtn(){
        tickBtn.removeFromSuperview()
        view.addSubview(tickBtn)
        let height = customisationView.frame.height - 35
        view.addConstraintWithFormat(format: "H:[v0(70)]-10-|", views: tickBtn)
        view.addConstraintWithFormat(format: "V:|-\(height)-[v0(70)]", views: tickBtn)
        tickBtn.addTarget(self, action: #selector(tickPressed), for: .touchUpInside)
    }
    
    func tickPressed(sender: UIButton){
        let bondTitle = bondTitleView.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if (bondTitle?.characters.count == 0){
            bondTitleView.shake()
        }
        else{
            //add new bond to coreData and send request to server
            createRoom()
            createBond(title: bondTitle!)
            let chatNav = UINavigationController()
            let chatLogVC = ChatLogViewController()
            chatLogVC.bondTitle = bondTitle
            chatNav.addChildViewController(chatLogVC)
            present(chatNav, animated: true, completion: nil)
        
            
        }
    }
    
    func creatRoom(){
        
    }
    func createBond(title: String){
        let context = DBManager.getContext()
        let bond = NSEntityDescription.insertNewObject(forEntityName: "Bond", into: context) as! Bond
        bond.title = title
            selectedContacts.forEach({ (contact) in
            bond.addToMembers(contact)
        })
        bond.createdOn = NSDate()
        DBManager.saveContext()
    }
    
    
    //MARK :- Collectionview delegate methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedContacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NewBondCell
        cell.checkBox.isHidden = true
        if let name = selectedContacts[indexPath.item].name{
            cell.nameLabel.text = name
            
        }
        cell.backgroundColor = Globals.sharedInstance.alternateBackgroungColor
        return cell
    }
    
}
